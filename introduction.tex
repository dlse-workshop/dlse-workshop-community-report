\section{Introduction}
\label{sec:introduction}


\subsection{The Interplay between Deep Learning \& Software Engineering}
\vspace{0.5em}

While DL systems bring with them the potential to tackle problem domains generally considered to be outside the realm of "traditional" programs, they represent a radical shift in the manner in which software is created. These changes encompass the emergence of new development workflows, new development artifacts, and new program properties that are, as of yet, not well understood. For example, DL models exhibit (i) a high degree of coupling between different model components, (ii) inherent stochasticity, (iii) distinct computational encodings, (iv) extreme sparseness in their representations of data, and (v) specifications that are inherently difficult to pin down. Because of this, many of the software engineering (SE) techniques and processes that have been developed and refined over decades for traditional software systems are not directly applicable to DL systems. For instance, techniques assuming deterministic behavior of software might fail due to the stochasticity of DL models whereas techniques that might assume a program is well-defined over most of its input domain (such as fuzzing) might fail due to the sparseness. Thus, there are clear challenges that exist related to understanding and supporting a new software development paradigm for DL systems, which we will refer to as Software Engineering for Deep Learning (SE4DL).

While challenges exist in adapting SE techniques and practices to DL systems, the capabilities that DL systems also  offer a tremendous opportunity to improve or automate several aspects of the "traditional" software development process. While DL provides a powerful solution to certain complex computational intelligence tasks, it is likely that "traditional" analytical programming methods will be more cost-effective for easily specifiable tasks, such as interfacing with a database. Thus, it would be preferable to automate or improve developer's effectiveness in these tasks. DL is poised to offer transformative techniques for traditional software development due to (i) the scale of software artifact data (e.g., code) in online repositories, (ii) the automated feature engineering provided by DL techniques, (iii) the robustness and scalability of optimization techniques such as gradient descent, and (iv) the transfer-ability of traditional DL applications (such as language modeling) to SE artifacts. These factors indicate the great potential for DL to tangibly improve the traditional software development process, and we refer to this area of research as Deep Learning for Software Engineering (DL4SE).



\subsection{Summary of Research Opportunities in SE4DL}
\vspace{0.5em}

In this report, we identify pertinent challenges related to \textbf{SE4DL} that were discussed at the workshop and motivate fundamental research to:

\begin{itemize}

	\item{\textbf{Expose the structure, variability, and characteristics of existing workflows for creating DL systems.} Decades of empirical research on the development workflows for traditional software engineering systems have provided a wealth of knowledge for such processes. However, given the nascent state of DL-based software systems, there is a need for additional empirical work with the aim of understanding emerging DL-based development workflows. Such work will require close cooperation between academic and industrial researchers, as indicated by early studies~\cite{Amershi:ICSE'19}.}
	
	\item{\textbf{Identify the abstractions inherent in DL workflows, models, and implementations which form the basis for SE of DL.} Much of the automated support for traditional software systems is driven by mature research and techniques for \textit{program analysis}.  These program analysis techniques are often built upon analyzing and manipulating abstract representations of traditional software programs (e.g. using abstract syntax trees or control-flow graphs). Thus, before work can begin in earnest on new classes of program analysis techniques for DL-based systems, researchers need to understand and define appropriate abstractions that are amenable to more advanced analysis and manipulation than model code or weights.}
	
	\item{\textbf{Identify modes of failure and their counter-measures within these workflows.} While failures and faults in traditional software systems tend to be quite varied, empirical research on understanding software bugs and testing has provided a rich knowledge base of faults across different types of systems. This has led to the formulation of automated techniques for identifying, reporting, triaging, and fixing such faults. However, the failure modes of DL-based systems currently are not well understood. For instance, faults may be highly coupled to the task a given model is employed for, and it may not be clear whether a given fault maps back to problems with a given model or the dataset the model learned from. Further research is needed to better understand the nature of faults for DL-based systems so that new techniques for testing and verification can be developed.}
	
	\item{\textbf{Establish theoretical foundations upon which cost-effective SE techniques for DL systems can be built.} Formal program analysis techniques have become a key building block for scalable and accurate analysis of software systems. However, proper theoretical underpinnings for DL-based software are needed in order to help help drive the next generation of these techniques. For example, defining implicit model-level property specifications is a necessary next step towards driving advancements in the verification and validation of DL-based systems.}
	
\end{itemize}

\subsection{Summary of Research Opportunities in DL4SE}
\vspace{0.5em}

Additionally, we identify several promising directions for future work related to \textbf{DL4SE} including:
\begin{itemize}
	\item{\textbf{Combining features learned from large-scale SE data with empirical human knowledge to more effectively solve SE tasks.} Decades of research on traditional SE processes and techniques has led to the development of a sizeable knowledge base regarding best practices for various tasks, attributes of effective tools, and understanding of human-centered processes. While DL techniques have shown immense potential in automatically learning features from large datasets, the empirical knowledge from the SE community should not be \textit{completely} ignored, as such synthesized knowledge may not necessarily be captured by DL techniques. Thus, there is need to develop creative techniques for combining the learned features of DL-based models with empirical knowledge to build more effective automated approaches. Such research could manifest the in the creation of labeled training datasets drawn from existing knowledge bases and taxonomies or in guiding DL techniques to learn specific features through targeted data pre-processing.}
	
	\item{\textbf{Leveraging heterogeneous sources of SE data (source code, requirements, issues, visual artifacts).} While code is generally regarded as the essence of a software system, in no way is it the only representation of software that developers handle in their daily workflows. In fact, software data is contained across three major information modalities: (i) \textit{code} represents source code and its corresponding abstractions;} (ii) \textit{natural language} manifests across a variety of software artifacts such as requirements, comments, issue trackers, bug reports, among others; finally (iii) \textit{graphical} artifacts are also abundant in software, comprising user interfaces and design documents among others. Given the inherent diversity among these representations, it stands to reason that these different information modalities capture \textit{orthogonal} properties or knowledge about a given underlying software system. Thus, future work should look for creative methods of combining these information sources together for richer DL-based representations of software.
	
	\item{\textbf{Developing tailored architectures that exploit the unique properties of SE data in order to offer better automated support to developers.} DL techniques have largely focused upon sequence-based learning for natural language corpora and spatial-based learning for graphical data. While such techniques can be applied "out of the box" for SE data such as code, requirements, or graphical data, artifacts of traditional software systems exhibit generally well-understood structural properties (e.g., control-flow in code and layouts of widgets in graphical user interfaces). As such, there is a need for researchers to develop techniques that take advantage of these structural properties in order to learn more effective DL-based representations of software artifacts for a variety of uses.}
	
	\item{\textbf{Defining a systematic and reproducible research methodology for DL applied to traditional SE tasks.} Reproducible and replicable research is the driving factor of scientific discovery. While all scientific disciplines face challenges related to the trade-offs of new discoveries versus the verifiability of past results, research involving DL techniques poses further challenges to reproducibility. Extremely large scale datasets, stochastic training processes, and variability in data preprocessing all contribute to unique difficulties in reproducibility of research related to DL4SE. Thus, rigorous research methodologies and transparency in the research process are essential.}

\end{itemize}

\subsection{Summary of Cross-Cutting Research Opportunities}
\vspace{0.5em}

Finally, this report outlines workshop discussions of several concerns, serving as topics for future work, that cross-cut both SE4DL and DL4SE:

\begin{itemize}

	\item{\textbf{Developing methods to explain how DL-based systems arrive at predictions.} Given their complex, high dimensional representations of data, it can be difficult to interpret a given prediction made by a modern DL-based software system. Such opaqueness complicates research that cross cuts both SE4DL and DL4SE. For instance, understanding common failure modes of DL systems will be difficult without some degree of model interpretability, furthermore, DL-based tools to automate traditional development tasks may be difficult to interpret in practice. Thus, interdisciplinary research on model explainability is needed.}

    \item{\textbf{Education for both students who will seek emerging positions that require engineering systems enabled by both DL and software technologies, and researchers who will study the inter-play of DL and SE.} Given the rapidly growing popularity of DL-based software systems, it is imperative that educational materials for both students and researchers are developed and widely disseminated. For students, coursework on both fundamental machine learning principles, as well as effective software engineering practices for DL-based systems will be important areas of curriculum development. For researchers, materials that offer guidance on rigorous empirical methods for carrying out research at the intersection of DL and SE are needed.}

    \item{\textbf{Community infrastructure for research in DL applied to SE, and for investigating the engineering fundamentals of DL systems.} A shared community infrastructure for managing a variety of DL-based artifacts such as models, code, and evaluation metrics would drastically improve the reproducibility of cross-cutting research.} 

    \item{\textbf{The need to foster a cohesive research community, involving both academics and industrial practitioners, in order to move the field forward.} The need for academic-industrial partnerships to advance several of the research directions above is immediately clear. For work on SE4DL, there is a need to study and understand current industrial practices and applications, and prove out new methodologies or tools in practice. For work on DL4SE, industrial datasets can be invaluable, as can access to evaluate DL-driven automation with real developers. Such partnerships should be a major goal for research moving forward.} 

\end{itemize}

\subsection{Report Structure}
\vspace{0.5em}

    The remainder of this report is structured in a manner that mirrors the breakout sessions of the 2019 NSF Workshop on Deep Learning and Software Engineering (Section~\ref{sec:workshop}). That is, each section following the introduction is dedicated to summarizing the discussion and crystallizing detailed directions for future work as specified by the attendees of the workshop. These directions are delineated in the text as discrete points labeled according to the overarching research topic in which they are situated. These sections largely expand upon the summary of research opportunities that are discussed above and in the \textit{Executive Summary}.