\section{Verification \& Validation of Deep Learning Systems}
\label{sec:verification}


\subsection{Background}
\vspace{0.5em}

Formal verification and validation approaches have become a key building block for scalable and accurate
analysis of software systems, but this did not happen over night.
For example, the first applications of symbolic execution to the validation of software
appeared in the mid 1970s, e.g., \cite{king1976symbolic,clarke1976system}, but it took three decades for the underlying
technologies, e.g., \cite{de:2008:z3,barrett-tinelli:2007:cvc3}, 
to mature to the point where researchers could push symbolic execution further
to apply it to realistic software systems or their components, e.g., \cite{khurshid-pasareanu-visser:2003:tacas,sen-etal:2005:cute,cadar2008klee}.
Today, these technologies are applied as a regular component of development workflows to validate software
and help verify the absence of faults and security vulnerabilities~\cite{DBLP:journals/cacm/CadarS13}.
We believe that developing cost-effective techniques for verifying and validating realistic deep
learning system models will require a sustained long-range program of research, and
we outline key elements of such a research program below.

\vspace{0.5em}
\subsection{Research Opportunities}
\vspace{0.5em}

\subsubsection{Property Specification}
\vspace{0.5em}

Verification and validation requires an explicit \textit{property specification} -- a precise
formal statement of the expected system behavior.
Specifying a property of a feed-forward DL model, $\bfN : \mathbb{R}^n \rightarrow \mathbb{R}^m$, involves the definition, $\phi \subseteq \mathbb{R}^n \times \mathbb{R}^m$, of the allowable inputs and their associated outputs.    
Since the process of specification is costly it is commonplace for verification and validation to 
exploit \textit{partial specifications}, which define necessary conditions for a system
to be correct.

Common sources of such specifications include the semantics of the programming language, e.g.,
freedom from null pointer dereference or out of bounds array indexing, or the
programming environment, e.g., reading is only permitted on a file that was
previously opened, in which the system is implemented.  Such \textit{implicit property specifications} are particularly valuable
because they are system-independent, for example, any system that dereferences
a null pointer is faulty.  This allows for such property specifications to be
formulated a single time and applied to verifying and validating any system, e.g., ~\cite{das2002esp,ball2004slam}.

DL models can suffer from faults related to violation of implicit specifications
in their implementations.   For example, the TensorFlow implementation of a model
might compute a NaN value when running inference on a particular input.  
Unlike existing work on
implicit specification, in this case such a violation may implicate
either the model or its implementation.   A key advance in validation and verification of DL models
will be to develop the analog of the ubiquitous ``freedom from null pointer dereference'' property for traditional 
software for DL models.

\begin{quote}
\textbf{VV$_1$:}\textit{ Research on defining implicit model-level property specifications for DL systems is needed to drive advances in and broad application of DL verification and validation approaches.}
\end{quote}

There is a rich literature on languages for stating \textit{explicit property
specifications}, e.g., assertions\cite{rosenblum1995practical}, 
contracts\cite{meyer1992applying,leavens1999jml}.
Such specifications are system-dependent and, consequently, they must be written
by system developers prior to verification and validation.

It is challenging to write specifications for traditional software, but the nature
of DL models introduces significant new challenges.   First and foremost, DL models
are  used when specifying the target function that the model aims to accurately
approximate is effectively impossible.   For example, specifying a Boolean classifier
for detecting the presence of a pedestrian in an image must account for 
the distance, orientation, occlusion, and natural variation of the shape and pose of a human --
not to mention myriad other factors.  Such models are thought to 
be \textit{inherently unspecifiable} which is surely true if one seeks a complete
specification of a DL model.
Yet researchers have made some progress by focusing on partial specifications, for example,
defining the maximum allowable steering angle for a regression network \cite{bojarski-etal:corr:2016:DAVE2}.

\begin{quote}
\textbf{VV$_2$:}\textit{ Research on defining explicit property specifications for DL systems should focus on partial specification of necessary conditions for correctness.}
\end{quote}

Despite the above challenges there has been progress in specifying properties of DL models.
Researchers have observed for some time that the continuity of a function means 
that \textit{metamorphic relations}, which state that small
changes in the input result in small changes in the output, should hold over
its input domain~\cite{liu2013effectively,murphy2008properties}.
The ML community has formulated a variety of such properties which 
are oriented towards determining a model's 
\textit{robustness to adversarial examples}~\cite{szegedy2013intriguing,goodfellow2014explaining,papernot2016practical}.   
Such specifications are sensible for regression models or when interpreting the values in the output layer of a categorical network.
However, when applied to the entire input domain robustness properties do not make sense for categorical models.
Such a property, e.g., $\forall i \in \mathbb{R}^n : \forall r \in [-\epsilon,\epsilon]^n : \bfN(i) = \bfN(i+r)$, implies by transitivity that the model can produce a single output value -- since the entire input domain can 
be spanned by overlapping regions of diameter $2 * \epsilon$.
This precludes classifiers that define a decision boundary and, consequently, the 
robustness is typically verified or validated on a very small sample of inputs relative to $\mathbb{R}^n$.

\begin{quote}
\textbf{VV$_3$:}\textit{ Research on understanding the value of metamorphic properties for categorical
networks that go beyond sampling would be valuable.}
\end{quote}

To address the challenge of writing property specifications, Ernst and colleagues developed
the concept of \textit{property specification inference} by observing system behavior and
generalizing it to a form that can be expressed as an assertion or contract.  Research
from the ML community on rule extraction, e.g., \cite{setiono1995understanding,bastani2018verifiable} to promote 
interpretability of models and explainability of their inferences shares a similar aim -- explicating
the complex inner workings of the system in a set of simpler partial specifications.
A first step towards property inference for DL models~\cite{gopinathASE19} has been made, however, this direction of work is still in its early stages.

\begin{quote}
\textbf{VV$_4$:}\textit{ Research on inferring concise specifications from model behavior that capture properties 
of models is a promising direction for overcoming the challenge of
specifying their behavior ahead of time.}
\end{quote}

\subsubsection{Adapting Validation to Deep Models}
\vspace{0.5em}

In the past two decades, a key advance in validating software systems has been the development of techniques
for forcing the execution or simulation of system behavior.  Whether
these techniques operate systematically, e.g., symbolic \cite{klee:osdi:2008} or concolic \cite{sen-etal:2005:cute} execution,
use randomized approaches, e.g., fuzzing \cite{afl}, their power lies in being fully automatic.
Algorithms and machines force the system through millions, or billions, of behaviors without developer
intervention.  In addition to reducing developer effort, this also eliminates bias that might cause
a developer to miss exercising a particular behavior and lead to latent system faults.   
Their ability to generate large numbers of behavior means, however, that is infeasible for developers
to determine whether the system output is correct -- there are simply too many behaviors to consider.
Consequently, these techniques rely on the availability of property specifications that can be encoded
into monitors that evaluate internal system states or externally visible system behavior relative to properties.

Researchers have begun the process of adapting these methods to DL models, e.g.,
\cite{sun-etal:ASE:2018,xie2019coverage,DBLP:conf/icml/OdenaOAG19,xie2019deephunter,DBLP:conf/icse/GopinathPWZK19}, but they would be much
more effective with a broad array of meaningful property specifications.
As a corollary to the research directions listed above

\begin{quote}
\textbf{VV$_5$:} \textit{ Research on automated test generation for deep models must evolve with and adapt to developments in property specification in order to maximize their impact in validating DL system behavior}
\end{quote}


\subsubsection{Scaling Verification to Realistic Models}
\vspace{0.5em}

It has taken nearly four decades to develop the foundations, algorithms, and efficient implementations for
verification and validation of realistic software systems \cite{DBLP:journals/cacm/CadarS13}.
The importance of DL models has led researchers to seek to adapt such approaches in recent years leading
to more than 20 different published verification techniques,
e.g., \cite{katz-etal:CAV:2017,DBLP:conf/atva/Ehlers17,8318388,ai2,DBLP:conf/ijcai/RuanHK18,NIPS2018_8278,tjeng2018evaluating,Bastani:2016:MNN:3157382.3157391,Dvijotham18,DBLP:conf/icml/WongK18,DBLP:conf/iclr/RaghunathanSL18,DBLP:conf/nips/WangPWYJ18,DBLP:conf/uss/WangPWYJ18,DBLP:conf/icml/WengZCSHDBD18,huang:2017:CAV,Boopathy2019cnncert,DBLP:conf/nfm/DuttaJST18,DBLP:conf/nips/BunelTTKM18},
spanning three major algorithmic categories~\cite{dnnverificationsurvey}.
The pace of innovation in DNN verification is promising, but to date these techniques cannot scale
to realistic DL models -- they either exceed reasonable time bounds or produce inconclusive results.
Consequently, in applying the techniques developers
restrict property specifications to very small fragments of the
input domain to gain a measure of tractability \cite{DBLP:conf/nips/WangPWYJ18,NIPS2018_8278,Dvijotham18},
restrict input dimension to facilitate verification  \cite{DBLP:conf/nips/WangPWYJ18,DBLP:conf/ijcai/RuanHK18}, and only consider networks
with a modest number of layers and neurons that do not reflect the rapidly increasing DNN complexity \cite{katz-etal:CAV:2017,DBLP:conf/nips/BunelTTKM18,katz2019marabou}.

Scaling verification and validation  for traditional software was achieved in large part through
the application of frameworks for \textit{abstracting system behavior} and performing \textit{compositional
reasoning} that divides the system into parts, reasons about them, and combines their results to 
reflect system behavior.

Several DNN verification approaches have explored the use of abstraction, e.g., \cite{singh-etal:POPL:2019:deeppoly,DBLP:conf/nips/WangPWYJ18}, to 
soundly over-approximate model behavior and thereby permit efficient verification.  As with traditional
software, the key to abstraction is to control the over-approximation so as to preserve the ability to
prove properties -- coarse over-approximation leads to inconclusive \textit{unknown} results in verification.
One framework for achieving this in traditional systems is \textit{counter-example guided abstraction
refinement} \cite{clarke2000counterexample} which systematically and incrementally customizes the abstraction based upon both
the property and the structure of the system being verified.

\begin{quote}
\textbf{VV$_6$:}\textit{ Research into abstraction refinement approaches for the verification and validation 
of DL models is needed to scale to realistic models while preserving the accuracy of verification results.}
\end{quote}

Reasoning about large software systems requires the ability to divide and conquer.  In data flow analysis,
this is achieved through sophisticated frameworks for inter-procedural analysis that summarize the behavior
of individual functions and then incorporate them into system level reasoning with just the right measure of context
to allow for accurate results \cite{nielson-etal:2005:principles-of-pa}.   
The research community has realized that ``whole program'' verification and validation 
of software is impractical and there is no reason to believe that  ``whole DL model'' verification
and validation will fare any better.
However, DL models are built out of components -- a graph of layers of varying type.
While the nature of these components and their interface to one another varies in significant
ways from traditional systems, the component-based nature of DL models may be ripe for exploitation.

\begin{quote}
\textbf{VV$_7$:}\textit{ Research into compositional verification and validation of DL models is needed to
scale to realistic DL models.}
\end{quote}

\subsubsection{Benchmarks and Evaluation}
\vspace{0.5em}

Advances in verification and validation techniques for traditional systems have
resulted from an ongoing interplay between theoretical and empirical work.
An analysis of progress in SAT solving over a period of 6 years convincingly demonstrates
how regular empirical evaluation of the state-of-the-art drives the consolidation of the
best ideas \cite{barrett20136}.
Benchmarks are a necessary ingredient in enabling such evaluation and other verification communities, e.g.,
for SMT \cite{barrett2010smt} and theorem proving \cite{tp-bench}, have long recognized this and invested in their development.

Most papers published on DL model verification and validation use only a small set of \textit{verification
problems} -- a pair of a model and a property specification.  For example, the ACAS network from the
landmark paper by Katz et al.~\cite{katz-etal:CAV:2017} is still used, e.g, \cite{katz2019marabou,gopinathASE19}, despite the fact
that it has orders of magnitude fewer neurons than realistic models, e.g., \cite{loquercio-etal:RAL:2018:dronet,bojarski-etal:corr:2016:DAVE2} -- 
not to mention that it only includes fully connected layers.   

\begin{quote}
\textbf{VV$_8$:}\textit{ Research on developing corpora of verification problems that represent important classes
of realistic DL models are needed in order to evaluate techniques and drive algorithmic and implementation 
improvements.}
\end{quote}

The existence of benchmarks is not enough.  The research community must agree to use them.
Appropriate incentives must be put in place to encourage this, for example, requiring that any work accepted
for publication perform direct comparison with alternate approaches on benchmarks.  In other verification
fields establishing yearly competitions has been successful in building such community 
expectations and in highlighting the state-of-the-art \cite{hw-comp,sat-comp,smt-comp,sv-comp}.

\begin{quote}
\textbf{VV$_9$:} \textit{ Researchers should consider establishing regular competitions for DL model verification and validation techniques.}
\end{quote}

\subsubsection{Design for Verification}
\vspace{0.5em}

It is well-understood that verification and validation approaches are undecidable in general, but
researchers have pursued them for traditional software because they need only be cost-effective for the software
that people write.   Taking this line of reasoning further, researchers in high-confidence systems have
placed restrictions on the structure of software systems that lend the results amenable to automated
verification and validation \cite{chapman2014we,holzmann2018power,crocker2007verification}.   To date DL research has focused primarily
on improving the test accuracy of models and, to a lesser extent, their robustness to adversarial
examples.  As DL model verification and validation matures and an understanding
of what types of model structures simplify and complicate verification there is an opportunity
to bias model architecture to facilitate verification.

\begin{quote}
\textbf{VV$_{10}$:} \textit{ Research exploring how model architecture facilitates or complicates verification and validation could pave the way for developers to design models for verification.}
\end{quote}

\subsubsection{Beyond Feed-forward Models}
\vspace{0.5em}

The research on DL model verification and validation described above has focused on feed-forward models, but
there are other DL paradigms, such as DRL and RNN, that have received some attention, but deserve much more.
For example, recent work has applied the concept of \textit{policy extraction} to DRL models to extract
an alternative model, e.g., a program fragment \cite{DBLP:conf/icml/VermaMSKC18}, a decision tree \cite{bastani2018verifiable}, that is
amenable to verification using existing techniques.

The sequential nature of these paradigms presents challenges for verification and validation, but these
are not unfamiliar challenges.   Distributed and concurrent programs exhibit the same characteristics
and research on their verification and validation gave rise to temporal logics for specification~\cite{mannaTLbook} and 
model checking~\cite{clarke-grumberg-peled:1999:book} for verification.

\begin{quote}
\textbf{VV$_{11}$:} \textit{ Research exploring how to adapt existing specification and verification frameworks
for reactive systems to sequential DL models will be needed to broaden the class of DL systems that are 
amenable to verification and validation.}
\end{quote}
