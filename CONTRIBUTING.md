## Overview

We invite open contributions to this report from the broader Deep Learning & Software Engineering Research Communities. Please use this as a guide on how to make contributions to the Community Report.

We are solciting commuinty invovlement in the report in the form of **Community Comments**. This means that while we will not allow for direct editing of the current report structure, in order to preserve the coherency of the collective discussions from the workshop, we will allow community members to comment or add to the content in a spearate section included at the end of the report.

## Contribution Process at a Glance

We will only be accepting comments written in LaTeX submitted via pull request to the [GitLab repository](https://gitlab.com/dlse-workshop/dlse-workshop-community-report). After a community memeber submits their comment via pull request at least two steering committee members will review the comments and approve them, merging the request into the report. This filtering process is largely to exclude comments that are out of scope or spam, the Steering Committee members will **not** reject any contributions that provide valid critical feedback or additional material to the report.

## Contributuing to the Report

### Create a GitLab.com Account

In order to contrinute to the report, you must have a registered account with GitLab, which is the collaborative service that we are using to host the report. 

You can create an account by visting the signup page located [here](https://gitlab.com/users/sign_up).

### Clone the Report Repo

The first step in making a contribution to the report is cloning the report GitLab repository. You can do this by visting the main page of the repo, and clicking on the "Clone" button on the right-hand side of the page and copying the repository address as highlighted in the image below.

![](img/cloning.png)

Once you have copied the address, you can clone the repo by using the `git clone <repository url>` command from the command line enviornment on your machine. This command may ask for your GitLab credentials.

### Write Your Contribution

Once you have cloned the repository to your machine you will have access to the latex files that constitute the report. 

#### Compiling the Report

Before you compile the report, you must install the LaTeX typesetting enviornment on your machine. This should be a fiarly simple process using binaires that are available for your OS ([Mac](http://www.tug.org/mactex/), [Windows](https://miktex.org), [Linux](https://www.tug.org/texlive/)).

Once you have LaTeX installed, you can compile the report by navigating to the folder where the `report.tex` file resides and execute the following commands, or open `report.tex` in your LaTeX editor of choice:

1. `pdflatex report.tex`
2. `bibtex -terse report.aux`
3. `pdflatex -pdf report.tex`
4. `pdflatex -pdf report.tex`

This should produce the report PDF titled `report.pdf`.

#### Formatting Your Contribution

In order to make your contribution, you should modify the `community-contributions.tex` and `references.bib` files **only**. Any merge requests that modify other files will be rejected. All content should be placed in the `community-contributions.tex` file whereas references should be placed in BibTeX format in the `references.bib` file.

You should use the following template to make your contributions:

```
\subection{Comments from <Author Name> - <Affiliation>}

\subsubsection{Heading 1}

<content and comments go here>

\subsubsection{Heading 2}

<more content and comments go here>

```

Following this template will allow for a cohesive set of community comments. If you need to deviate from this formatting for any reason, please idicate the deviation and purpose in the description of the pull merge request. 

Feel free to include tables and figures in your comments if they add value to your discussion. Figures should be placed in the `img` folder of the repo, and tables can be included inline with the LaTeX content.

### Submitting a Merge Request

#### Committing and Pushing Your Comments to GitLab

Once you have finished wirting your comments, you should then create a merge request with your changes. To do this, first you need to create a new branch in your local cloned repository. To this, you can use the following command:

`git checkout -b <branch-name>`

where the branch name should be in the form `<author-name>-comments`. Once you have created the branch, you can then commit your changes using the following command:

`git commit -a -m "<commit message"`

(Note that this will commit **all** local changes to the branch you created). 

After you have committed your changes to you local repo and branch, you must then push this to the remote GitLab repo in order to create a Merge Request. To do this, you can use the following command:

`git push origin <branch-name>`

This will then upload your branch with your comments to the remote GitLab repo. (Note that this step may ask you for your GitLab username and password).

#### Creating a Merge Request on GitLab.com

Now that your changes have been pushed to GitLab in the form of your comments branch, you need to submit a Merge Request to notify the steering committee to review changes and merge them into the main brnach of the repo. To do this you should select the Merge Request option from the GitLab side bar (on the left-hand side of the screen) as highlighted below in red:

![](img/merge-1.png)

Then on the next page you should click the "New Merge Request Button", as depicted below:

![](img/merge-2.png)

On the next page you should must select the source and target branch from the dropdown menus in order to complete the Merge Request. In the source branch selection box (indiciated in blue below) select your branch with comments that you pushed to GitLab. In the target branch seelction box (inidicated in red below) ensure that the "master" branch is selected. 

![](img/merge-3.png)

After you have finished this, click the "Compare branches and continue" button. On the next page, enter a title for your merge request in the form of `<Author Name> - <Affiliation> - Community Contribution`. Then enter a breif summary of your contribution in the description box. You should then fill out the "Assignee", "Milestone", and "Labels" as indicated in the image below. That is assign the request to the user @kpmoran, assign **no** Milestone to the report, and then add the "community contribution" label. 

Finally ensure the "Delete source branch when merge request is accepted" option is checked. After this, you are ready to submit the reqeust and finish your contribution process!


![](img/merge-4.png)


## Troubleshooting and Questions

Please email kpmoran [at] cs.wm.edu with any questions or concerns regarding the contribution process. Alternatively please file an issue report with this repo. 