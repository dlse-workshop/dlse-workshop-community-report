\section{Deep Learning for Code Generation}
\label{sec:dlcodegen}

\subsection{Background}
\vspace{0.5em}

The task of program synthesis, i.e., automatically generating programs from specifications, is considered a fundamental problem in Artificial Intelligence. In recent years, there has been tremendous research progress in the field across various communities including Software Engineering, Programming Languages, and Artificial Intelligence. The advances in deep learning techniques coupled with neuro-symbolic reasoning techniques offer exciting opportunities to enable new automated code generation paradigms and interfaces for programmer-synthesizer collaboration.

\subsection{Research Opportunities}
\vspace{0.5em}

\subsubsection{Application Domains}
\vspace{0.5em}

There are a number of different application domains that might be well suited for automated code generation both in near term and longer term research. Some near term opportunities may lie in the areas of:
\begin{itemize}
    \item{\textbf{Program Superoptimization} -- automatically generating programs that are functionally equivalent to a given implementation but allow for complete transformation of original programs using search unlike traditional compilers.}
    \item{\textbf{Code completion} -- generating completions of small snippets of code given some code context.}
    \item{\textbf{Repairing programs with small program patches} -- Instead of generating complete code snippets, synthesis techniques can be used to generate small program patches that satisfy the failing specification or tests.}
    \item{\textbf{End-user programming} -- Helping millions of end-users who may not necessarily be programmers like spreadsheet users to accomplish programmatic tasks.}
    \item{\textbf{Mobile app development} -- Helping programmers develop mobile applications using natural language and examples.}
\end{itemize}


Another area of interest was for synthesizing programs in domain-specific languages such as SQL, yaml, and build files was identified as a promising opportunity. In these domains, programmers typically have knowledge about performing tasks in general purpose languages, but often have to look up the syntax and semantics of these domain-specific languages. There could be opportunities in considering natural language as intermediate representations for different computations and use that to transfer implementations in different domain-specific languages. One research challenge here would be on how to enable naturalness and readability of the automatically generated code for maintainability if developers use them as part of a larger workflow.

\begin{quote}
\textbf{CG$_1$:}\textit{ There a number of application domains that represent promising paths forward in DL for code generation including (i) program superoptimization, (ii) code completion, (iii) program repair, (iv) end-user programming, and (v) mobile app development. However, major challenges remain in ensuring that generate code is both easily comprehendable and maintainable.}
\end{quote}

A key challenge in program synthesis is that of specification, particularly for complex tasks. Even for simple programs, writing a full specification can sometimes be as tedious as writing the complete program in first place. There are several alternate specification mechanisms such as natural language, input-output examples, unit tests, partial implementations, existing implementations, program properties, and user interfaces, where different mechanisms are suited for different synthesis domains. One big opportunity with advances in deep learning techniques is to enable a rich environment that can embed truly multi-modal specifications in various forms as listed above. One particular specification mechanism that may be promising was to start with an existing implementation of code found with some keyword based search and learn to edit it in ways to satisfy the specification. Another important challenge to keep in mind here is that the expertise of users might also influence the types of specification mechanisms that are useful in practice -- e.g. inexperienced users might not be familiar with even good keywords.

\begin{quote}
\textbf{CG$_2$:}\textit{ Combining multi-modal DL models that are capable of mixing and taking advantage of different types of program specifications represent a promising path forward for research on code generation. However, researchers should also keep in mind the context of where the approach will be applied and ensure generation mechanism matches the user expertise.}
\end{quote}

Instead of fully automating the code generation process, we can also consider building an assistive agent for helping developers and focus more on the collaborative and creative aspects of development. Human developers are great at certain aspects of code development workflow and code generators would be good at certain other complementary aspects such as remembering large contexts. Combining the two sets of varying expertise would be an exciting research challenge. Proactive code completion is one such example, where a code assistant can recognize whether certain functionality is incomplete and suggest useful idioms or completions. Writing code is seldom a one-shot process even for human developers -- we usually iterate quite a few times before leading to a final implementation. One interesting challenge would be building a mixed-initiative dialog agent that switches between developer and code generator while asking questions to refine the intent and generating the desired implementation.

\begin{quote}
\textbf{CG$_3$:}\textit{ Researchers should carefully consider the interplay between a code generator and end-user developers, and develop mixed-initiative agents that are capable of working in concert with developers, rather than trying to automate end-to-end development tasks outright.}
\end{quote}

There is also a strong need for benchmarking the progress of the capability of program synthesizers as well as possibly developing some grand challenge problems for the community. One particular grand challenge was to build an intelligent synthesizer that can win a programming contest. Some other suggestions for grand challenge problems included building a synthesizer to pass an introductory programming class, enabling students to automatically generate mobile apps, and automatically answering questions on help forums such as StackOverflow. Evaluating synthesizers that require human interaction is also a complex question, and coming up with metrics to quantify and benchmarking such interactions would also be an interesting direction.

\begin{quote}
\textbf{CG$_4$:}\textit{ There is a need for the research community to develop comprehensive benchmarks or "grand challenges" for code generation to measure and spur progress (akin the imagenet challenge in computer vision research).}
\end{quote}

There are also many interesting research challenges in developing deep learning architectures that are specialized for embedding programs and specifications. Some recent models have shown that encoding structure and static analysis information about programs is useful for improving the generative process of programs. In addition, developing models that are capable of embedding dynamic and rich semantic information would be crucial for improving current program synthesis models.

\begin{quote}
\textbf{CG$_5$:}\textit{ There is a need for DL models that are able to specifically take advantage of the inherent static structure and dynamic properties of source code.}
\end{quote}

In summary, the advances in DL techniques offer exciting opportunities for building systems that can understand multi-modal specifications in various forms such as natural language, input-output examples and partial programs. Combining DL techniques with symbolic approaches can significantly improve the capabilities of current synthesizers by combining intuition-based reasoning with symbolic logical reasoning. This is an exciting area of research with many open challenges and fundamental problems, and can also be a pillar for advancing DL techniques.
