\section{Maintenance of Deep Learning Systems}
\label{sec:maintenance}


\subsection{Background}
\vspace{0.5em}

Software maintenance is a key phase of the software development life-cycle wherein a system is modified to correct faults, add features, or to improve various other functional or non-functional properties. It has been estimated that nearly half of all software engineering activities are dedicated to one of the various types of maintenance activities~\cite{Lientz:1980}. As such, there has been a tremendous amount of research effort involved in analyzing and improving maintenance-related software engineering tasks. Such work has ranged from automated analysis of issue and bug reports~\cite{Moran:FSE'15,Chaparro:ICSME'16,Chaparro:ICSME'17,Moran:ICSE'16}, to full-fledged automated repair of faults in software systems~\cite{LeGoues-ICSE'12,Weimer:ICSE'09,Chen:TSE'19}.  However, until recently, much of this work has been focused primarily upon \textit{traditional} software systems written in an analytic nature. As noted at the beginning of this report, as software is applied to tackle increasingly complex tasks, there has been a shift from analytical development to \textit{learning-based} development, where machine learning algorithms are applied to large datasets to ``learn'' a program for a given computational task. The popularity of multi-layered Neural Networks and accompanying optimization algorithms (so-called Deep Learning architectures) have been a major driver of this phenomenon. However, such learning-based systems are inherently different from, and often intertwined with, more traditional software systems. Given the relative recency of DL-based software systems, there are many open questions regarding proper maintenance practices. We believe that developing and ensuring proper maintenance practices for DL-based systems is imperative, and we outline the key elements of a proposed research agenda below.

\subsection{Research Opportunities}
\vspace{0.5em}

\subsubsection{Measuring and Understanding the Evolution of Deep Learning Systems}
\vspace{0.5em}

The maintenance of a software system is tightly coupled to its evolution, i.e., the types and magnitude of maintenance performed and software artifacts present in a system often dictate how that system evolves over time. Different from more traditional software systems, DL systems have additional artifacts that must be accounted for during software evolution. For instance, such systems will typically consist of (i) model code that implements a given DL architecture, (ii) configuration files that specify different model hyper-parameters, (iii) trained models, (iv) datasets split for various use cases (\eg training/validation/test sets), and (v) performance or effectiveness metrics for different trained models. 
The process for constructing DL systems also fundamentally differs from more traditional software systems. DL-based systems are inherently more ``experimental'' in nature, wherein developers will construct, train, test, and tweak several different models and DL architecture configurations. Given the opacity of DL models, and the sometimes surprising nature of model results, engineers often implement a more exploratory process compared to more traditional software systems where the behaviors of analytical code are easier to predict. Given the differences in software artifacts and development pipelines between DL-based systems and traditional software systems, we would expect there to be distinct differences in their evolution and maintenance as well.


However, currently the software engineering research community is still grappling with the differences in process and evolutionary aspects of DL-based systems. We do not know how to precisely track
changes nor do we understand how they are typically manifested.

As indicated earlier, ML/DL-based systems are heavily data-centric from the viewpoint of developers~\cite{Amershi:ICSE'19}. That is, data is a crucial, and often unwieldy, component that enables DL architectures to learn ``programs'' for complex applications. However, the current state of data management for DL systems is a pain point for many developers, and thus should be a focus area for the research community. However, data is not the only artifact that must evolve over the lifespan of a DL-based software system. There are several interconnected artifacts such as trained models, model test results, hyper-parameter configurations, and model code that must all co-evolve in an efficient manner. The intermingling of these artifacts often results in a large amount of glue-code coupling these artifacts together. Designing both processes and techniques/tools to help measure  such co-evolution should be a focus of researchers moving forward to start taming this challenge.
\vspace{0.5em}

\begin{quote}
\textbf{M$_1$:}\textit{ Researchers should focus on designing techniques for efficient tracking and evolution of the rich and often tightly coupled software artifacts that are associated with DL-based systems.}
\end{quote}

A recent survey conducted at Microsoft has provided some insight into the processes by which ML/DL components are created and integrated into existing software systems~\cite{Amershi:ICSE'19}. This work has found three main aspects of ML/DL-based software systems that fundamentally differ from other domains including: (i) discovering, managing, and versioning data, (ii) model customization and reuse, and (iii) the integration of AI components into more traditional software systems. However, while this study begins to scratch the surface of evolutionary aspects of DL systems, there are still many open questions that remain, for instance: \textit{What does change look like in a DL-based system?}, \textit{How do models and datasets co-evolve?}, \textit{How important is it to maintain a version history for datasets?}, \textit{How does model code co-evolve with trained models and testing?}, \textit{Do different types of regressions befall DL-based systems?}. To answer such questions it is clear that additional empirical studies are needed to help guide eventual research towards supporting developers with some of the more challenging aspects of such evolution.

\begin{quote}
\textbf{M$_2$:}\textit{ Research empirically analyzing the evolutionary properties of DL-based systems will provide much needed direction towards understanding and eventually supporting developer needs throughout the software maintenance process.}
\end{quote}



\subsubsection{Grappling with Technical Debt and Maintenance in Deep Learning Systems}
\vspace{0.5em}

Due to the rapid pace of development typically associated with modern software systems, engineers are often seen as facing a tenuous dichotomy: move quickly and ship new features and completed projects or slow down to ensure the quality of engineering and the sound design and implementation of a system. This trade off between sound engineering practices and velocity of progress is often referred to as a concept called \textit{technical debt}. Technical debt shares several aspects in common with fiscal debt. For example, if poor engineering decisions are made for the sake of development speed and  are not corrected in a timely manner, this could result in increasing maintenance costs. Technical debt is an increasingly researched topic within the broader field of software engineering~\cite{Verdecchia:TechDebt'18}. However, currently there is only an early understanding regarding the types of technical debt incurred specifically by DL-based systems. Recent work from engineers and researchers at Google has shed light on the various forms of debt that more general ML systems can incur~\cite{Sculley:NIPS'14}. Some of the types of debt explored by this work include (i) the erosion of boundaries between software ML software components, (ii) System-level spaghetti, and (iii) changes in the external world (and representing data). While these types of debt generally apply to DL-based systems, this paper was a wider look at ML-based as a whole.  We are also witnessing the emergence of techniques such as refactoring, inspired by those in traditional software, but adapted to work on DL-based systems to address different types of technical debt \cite{shriver2019RefactoringNN}. 
There are likely to be even more  forms of technical debt that could be incurred by DL-based systems, for instance: \textit{What is the impact of a change to the data or the model?}, \textit{Are the changes to maintain accuracy diminishing robustness?}, and \textit{What is the effect of using an existing off-the shelf architecture on explainability?}.
Thus, there needs to be better understanding of the specific types of technical debt that DL systems may incur.

\begin{quote}
\textbf{M$_3$:}\textit{ Research into the trade-offs between development velocity and maintenance costs should be undertaken to empirically determine mechanisms to help developers manage such debt.}
\end{quote}



\subsubsection{Abstractions to enable Analyses}
\vspace{0.5em}

DL-based systems have unique computational encodings that render existing traditional  software abstractions largely obsolete. For instance, it is unclear how traditional control and data flow would map to  DL architectures  made up of several different computational layers that engineers may view as performing discrete functions. However, understanding these abstractions will be key towards enabling automated analysis of such systems in the future.

\begin{quote}
\textbf{M$_4$:}\textit{ Researchers should strive to understand the salient abstractions of DL-based systems, both mental and technical in hopes of better supporting analyses of DL-based systems.}
\end{quote}

Once a proper set of abstractions have been established, it is critical that work be conducted in order to aid in the automated analysis of DL-based programs. Decades of research on program analysis techniques of more traditional software systems have ushered in several advancements in automated developer tools and frameworks for software maintenance and validation. If we hope to achieve similar levels of advancement in the context of DL-based systems, researchers must work toward designing and building these next-generation analysis techniques. This could come in the form of instrumentation of dynamically running DL models, or via a combination of static analysis of model configuration and dynamic analysis. 

\begin{quote}
\textbf{M$_5$:}\textit{ Research into program analysis tools for DL-based systems is critical if the research community is to develop automated developer support for software maintenance tasks.}
\end{quote}


\subsubsection{Educational Challenges for Teaching Deep Learning System Maintenance}
\vspace{0.5em}

As with any emerging domain of software development, education is a critical component to ensuring engineers are equipped to work with DL-based systems in as effective and efficient a manner as possible. However, from a researchers perspective, it can be difficult to glean best-practices and in turn develop effective pedagogical mechanisms for conveying these to students. One potentially promising path forward for tackling the current gap in computer science education is for researchers/educators to collaborate with industry to learn current best practices and co-develop course materials and pedagogical techniques for conveying it.

\begin{quote}
\textbf{M$_6$:}\textit{ Researchers and Industrial practitioners should work together to identify effective development/maintenance practices for DL-based systems and co-design educational and pedagogical materials for conveying these to students.}
\end{quote}
