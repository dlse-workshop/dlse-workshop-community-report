![](img/title.jpg)

## Overview

This repo holds the community report summarizing the discussion and roadmap of future that sits at the intersection of DL and SE, resulting from the [NSF Workshop on Deep Learning & Software Engineering](https://dlse-workshop.gitlab.io) held in San Diego California on Nov 10th-12th.

As per the goals of the workshop, we intend the workshop report to be an open living document that outlines promising future work that sits at the intersection of Deep Learning and Software Engineering. Therefore, we invite community contributions to this document. Please see this [contribution guide](/CONTRIBUTING.md) for more information on how to contribute.

## Download Latest Version of the Report

The report is autaomtically compiled using GitLab's CI/CD system. You can download the latest version of the PDF by clicking below, and the latest source by cloning the repository.

*  [Click Here to Download the Latest Report PDF](https://gitlab.com/dlse-workshop/dlse-workshop-community-report/-/jobs/artifacts/master/download?job=initial_compile)

-----

## Organizing Committee

![](img/Denys.png)  
 
[Denys Poshyvanyk](http://www.cs.wm.edu/~denys/index.html), William & Mary 


![](img/Baishakhi.png) 

[Baishakhi Ray](http://rayb.info), Columbia University

-----

## Sponsor

![](img/Sol.png) 
 
[Sol Greenspan](https://www.nsf.gov/staff/staff_bio.jsp?lan=sgreensp&org=NSF&from_org=NSF), National Science Foundation

-----

## Steering Committee

![](img/Prem.png) 

[Prem Devanbu](https://web.cs.ucdavis.edu/~devanbu/), University of California at Davis

![](img/Matt.png) 

[Matthew Dwyer](https://matthewbdwyer.github.io), University of Virginia

![](img/Mike.png) 

[Michael Lowry](https://ti.arc.nasa.gov/profile/lowry/), NASA

![](img/Xiangyu.png) 

[Xiangyu Zhang](https://www.cs.purdue.edu/homes/xyzhang/), Purdue University

![](img/Rishabh.png) 

[Rishabh Singh](https://rishabhmit.bitbucket.io), Google

![](img/sebastian.png) 

[Sebastian Elbaum](http://www.cs.virginia.edu/~se4ja/), University of Virginia

![](img/Kevin.png) 

[Kevin Moran](https://www.kpmoran.com), William & Mary

-----

## Citation

~~~
@inproceedings{dlse19-report,
  title={Deep Learning & Software Engineering: State of Research and Future Directions},
  author={Prem, Devanbu and Dwyer, Matthew and Elbaum, Sebastian and Lowry, Michael and Moran, Kevin and Poshyvanyk, Denys and Ray, Baishakhi and Singh, Rishabh and Zhang, Xiangyu},
  booktitle={Proceedings of the 2019 NSF Workshop on Deep Learning and Software Engineering},
  year={2019}
}
~~~
