%\vspace{-3em}
\noindent{\Large \textbf{Executive Summary}}
\vspace{1em}

The advent of deep learning (DL) has fundamentally changed the landscape of modern software. Generally, a DL system is comprised of several interconnected computational units that form "layers" which perform mathematical transformations, according to sets of learnable parameters, on data passing through them. These architectures can be ``trained'' for specific tasks by updating the parameters according to a model's performance on a labeled set of training data. DL represents a fundamental shift in the manner by which machines learn patterns from data by automatically extracting salient features for a given computational task, as opposed to relying upon human intuition. These DL systems can be viewed as an inflection point for software development, as they enable new capabilities that cannot be realized cost-effectively through "traditional" software wherein the behavior of a program must be specified analytically. This has ushered in advancements in many complex tasks, often associated with artificial intelligence, such as image recognition, machine translation, language modeling, and recently, software engineering. DL is fundamentally intertwined with software engineering (SE), primarily according to two major themes.

 The first of these two themes is related to DL-techniques when viewed as a new form of software development, and in this report we refer to this as \textbf{Software Engineering for Deep Learning (SE4DL)}. In essence, the application of DL to a computational problem represents a new programming paradigm: \textit{rather than analytically specifying a program in code, a program is "learned" from large-scale datasets}. This new form of development carries with a new set of challenges that represent several opportunities for novel research.

The second of these two themes is related to leveraging Deep Learning techniques in order to automate or improve existing software development tasks, which we refer to as \textbf{Deep Learning for Software Engineering (DL4SE)}. There currently exists an unprecedented amount of software data that is freely available in open source software repositories. This data spans several different types of software artifacts, from source code and test code, to requirements and issue tracker data. Given the effectiveness by which DL systems are able to learn representations from such large-scale data corpora, there is ample opportunity to leverage DL techniques to help automate or improve a wide range of developer tasks. However, with this opportunity also comes a number of challenges, such as curating datasets to develop techniques for particular development tasks, and designing DL models that effectively capture the inherent structure present in a wide range of different software artifacts.

Given the current transformative potential of research that sits at the intersection of DL and SE, an NSF-sponsored community workshop was conducted in co-location with the 34th IEEE/ACM International Conference on Automated Software Engineering (ASE'19) in San Diego California. The goal of this workshop was to outline high priority areas for cross-cutting research that sits at the intersection of Deep Learning and Software Engineering. While a multitude of exciting directions for future work were identified, we provide a general summary of the research areas representing the areas of highest priority which are expanded upon in Section \ref{sec:introduction}. The remainder of the report expands upon these and other areas for future work with high potential payoff.

\vspace{1em}
\noindent \textbf{\large Community Research Challenges}
%\vspace{-1.1em}

\begin{itemize}
    \item{\textit{\textbf{Laying the Foundations for SE4DL including defining DL workflows, proper DL abstractions, and theory for DL development.} - }  A systematic taxonomy and understanding of different development workflows for DL-based systems and how these differ from traditional software development practices is needed. Researchers should also look to develop salient DL program abstractions that can be easily constructed and analyzed in a formal manner.}
    \item{\textit{\textbf{Identifying failure modes of DL Systems and their corresponding countermeasures} - } There is a dire need for a detailed categorization and understanding of common types of faults for DL-based systems alongside automated techniques for detecting, debugging, and fixing such faults.}
    \item{\textit{\textbf{Furthering DL4SE through the development of tailored architectures, use of heterogeneous data sources, focusing on new SE tasks, and combining DL with existing empirical data.} - } New DL models designed specifically for given SE tasks that learn orthogonal information from a heterogeneous software artifacts while making use of the specific structural properties of such artifacts. Furthermore, researchers should look for ways to combine existing empirical knowledge into approaches for DL4SE, examine new categories of tasks, and look towards incorporating multi-modal data representations.}
    \item{\textit{\textbf{Establishing educational and pedagogical materials to better support training related to DL-based development via academic-industry partnerships} - } While progress is being made on understanding DL-based development workflows, there should be parallel effort dedicated to developing effective pedagogical and educational material to transfer newly discovered knowledge on to students. Industry-academic partnerships could aid in ensuring the immediate impact of such material.}
\end{itemize}


\begin{comment}

\begin{itemize}
    \item{\textit{\textbf{Expose the structure of characteristics of workflows and identify the inherent abstractions of DL systems to aid analysis} - }  A systematic taxonomy and understanding of different development workflows for DL-based systems and how these differ from traditional software development practices. Salient DL program abstractions that can be easily constructed and analyzed in a formal manner.}
    \item{\textit{\textbf{Identify failure modes of DL Systems and their corresponding countermeasures} - } A detailed categorization and understanding of common types of faults for DL-based systems alongside automated techniques for detecting, debugging, and fixing such faults.}
    \item{\textit{\textbf{Combine heterogeneous sources of SE data in tailored models and automatically learned features with existing empirical knowledge} - } New DL models designed specifically for given SE tasks that learn orthogonal information from a heterogeneous software artifacts while making use of the specific structural properties of such artifacts.}
    \item{\textit{\textbf{Develop techniques to better understand predictions made by DL systems} - } A series of program analysis techniques based upon the developed underlying abstractions and workflows of DL-based systems, that can aid in explaining model behavior to developers.}
\end{itemize}

\begin{table*}[h!]
\begin{tabular}{|p{5cm}|p{10.6cm}|}
\hline
\rowcolor[HTML]{C0C0C0} 
{\color[HTML]{333333}\textbf{Challenge}} & {\color[HTML]{333333}\textbf{Research Vision}} \\ \hline
\textit{Expose the structure of characteristics of workflows and identify the inherent abstractions of DL systems to aid analysis} &  A systematic taxonomy and understanding of different development workflows for DL-based systems and how these differ from traditional software development practices. Salient DL program abstractions that can be easily constructed and analyzed in a formal manner.\\ \hline
\textit{Identify failure modes of DL Systems and their corresponding countermeasures} & A detailed categorization and understanding of common types of faults for DL-based systems alongside automated techniques for detecting, debugging, and fixing such faults. \\ \hline
\textit{Combine heterogeneous sources of SE data in tailored models and automatically learned features with existing empirical knowledge} & New DL models designed specifically for given SE tasks that learn orthogonal information from a heterogeneous software artifacts while making use of the specific structural properties of such artifacts. \\ \hline
\textit{Develop techniques to better understand predictions made by DL systems} & A series of program analysis techniques based upon the developed underlying abstractions and workflows of DL-based systems, that can aid in explaining model behavior to developers. \\ \hline
\end{tabular}
%\vspace{-0.3cm}
\caption{High priority Research Areas Identified}
\label{tab:directions}
\end{table*}
\end{comment}

\vspace{1.5em}
\noindent \textbf{\Large \img{img/nsf-logo.png} Acknowledgement of Sponsorship}
\vspace{1em}

This material is based upon work supported by the NSF under Grants No. CCF-1927679 and CCF-1945999. Any opinions, findings, conclusions, or recommendations expressed in this material are those of the authors and do not necessarily reflect the views of the NSF. We would also like to specifically acknowledge and thank Dr.~Sol Greenspan, sponsoring NSF program director, for support and guidance through the planning and execution of the workshop.

 

