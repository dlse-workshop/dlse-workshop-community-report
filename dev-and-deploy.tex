\section{Development \& Deployment Challenges for Deep Learning Systems}
\label{sec:dev-deploy}

\subsection{Background}
\vspace{0.5em}

As DL continues to pervade modern software systems, developers and data scientist are beginning to grapple with development and deployment challenges that are markedly different from traditional processes. While the development process for traditional software systems has gone through many iterations (e.g., waterfall $\rightarrow$ agile), the steps in these process are generally fairly well-defined. However, for DL-based systems, the process is much more "experimental" in many regards. That is, developers must formulate hypotheses regarding their problem and dataset, create models, and "test" these models against their hypothesis to determine if they are capturing trends in the data effectively and making relevant predictions. Given the relatively experimental nature of the DL development process, there are many open questions regarding effective best-practices, tooling, and sociotechnical processes that represent promising areas for future work.

\subsection{Research Opportunities}
\vspace{0.5em}

\subsubsection{Requirements Engineering for DL Systems}
\vspace{0.5em}

Requirements engineering is a critical part of any software development process, as specifying what should be built can often be more difficult than the process of actually instantiating the ideas into code. However, the experimental nature of DL-based systems can make requirements more difficult to pin down. When used in industry, engineers often don't have formal requirements for a DL system~\cite{Amershi:ICSE'19}. This is due to the fact that, if a team is turning to a DL-based solution for a specific problem, this means that the problem domain is likely too complex to specify analytically. Therefore, teams often have a general goal in mind, and a specific success criteria for that goal. For most DL-based systems this goal is often exceeding a particular threshold for a given effectiveness metric drawn from ML literature, and when this threshold is met, further "optimal" performance is rarely sought after the fact. However, there are two main aspects of requirements engineering for DL systems that are potentially important and serve as areas of future work: (i) delineating and understanding the boundaries of performance of a given model, and (ii) non-functional requirements such as model size and inference time. In order to understand whether the predictive performance a given DL-model satisfies the requirements of a given problem, the limits and boundaries of these models must be explored and understood.

\begin{quote}
\textbf{DD$_1$:}\textit{ Research on developing techniques to properly specify the behavioral boundaries and limitations of DL-models will be an important aspect of requirements engineering to ensure proper effectiveness for a given problem domain.}
\end{quote}

In addition, given the problem domain, there be important non-functional requirements related to aspects such as model size, inference time, privacy and bias considerations, or memory size. Developers will require support in ensuring such requirements are met in practice.

\begin{quote}
\textbf{DD$_2$:}\textit{ Developers will need automated support for building DL-based systems that meet a variety of non-functional requirements including technical considerations such as model size, and non-technical considerations such as issues with privacy or bias.}
\end{quote}

In addition to requirements engineering, the notion of software traceability is also in its nascent stages for DL-based systems. In traditional software systems, the artifacts typically involved in the traceability process are usually well-defined, and often carry with them an inherent structure. However, this is not necessarily the case for DL-based systems. For instance, one could envision traceability tools that trace from data examples to different abstract data representations within a given model. The data-driven nature of DL also poses new challenges and opportunities for work on traceability. For example, given that understanding and cleaning data is such a large part of the DL development pipelines, traceability approaches could be reworked to offer links between various clusters of a given dataset and testing examples.

\begin{quote}
\textbf{DD$_3$:}\textit{ There is a need for traceability to be fundamentally rethought for DL-based systems. Researchers should focus on determining what types of trace links are necessary for DL-based systems and work toward automated approaches that can automatically infer and reason about such links.}
\end{quote}

%\subsubsection{Monitoring the Evolution of DL Systems}
%\vspace{0.5em}

%Software artifacts are rarely stagnant, instead evolving over time as the requirements and target problem domain change. While evolutionary processes for traditional software systems have been studied at length in SE literature, the data-driven nature of DL-based systems makes evolution challenging. Dl-models consist of several disparate pieces, including datasets, model code, trained models, and training/evaluation results that can make it difficult to track co-evolution of the artifacts. Without some form of tracking of this co-evolution, it can be difficult for developers to understand changes in model behavior and reason about various configurations of models and data, ultimately complicating the development process. Thus there is a clear need for monitoring mechanisms that are able to track and support the co-evolution of different types of DL-based artifacts.

%\begin{quote}
%\textbf{DD$_4$:}\textit{ Research related to mechanisms and processes for monitoring and co-evolving the software artifacts connected to DL-based systems is needed.}
%\end{quote}

\subsubsection{Sociotechnical Aspects of DL System Development}
\vspace{0.5em}

At its core, any type of software development is a process that is carried out by humans, and more often than not, groups of humans working together toward a common goal. Given this fact, the socio-technical processes of development are critical to successful creation and instantiation of software. With the rise of agile methodologies, and collaborative tools (e.g., issue trackers, software specific task managers) robust methods for cooperative development of traditional software systems have been developed. However, the experimental nature of DL-based systems and their differing iterative processes make it difficult to cleanly transfer many of the existing development processes and tools. 

\begin{quote}
\textbf{DD$_4$:}\textit{ Researchers and practitioners should work together to study and understand effective processes and tools for the development of DL systems. This will provide guidance for future work on more intelligent tools that improves or accelerates these processes.}
\end{quote}

In work that has examined the DL-like development processes carried out by data scientists~\cite{Amershi:ICSE'19}, it is clear that the most time- and effort-consuming portion of the development process is data curation and management. As the popularity of DL-systems have continued to improve, libraries and APIs for creating models have become much easier to use. However, these underlying models only function well if trained on properly curated datasets. Therefore, much of the engineering effort for such systems is focused on data. This is a fundamental shift from more traditional software development workflows, wherein the focus is largely on code. Instead, developers and data scientists must focus on \textit{understanding} data, and making sure that the data accurately reflects the problem that they are trying to solve, to extent that is possible. This means that various types of exploratory data analysis should be given the same levels of consideration as program analysis techniques have been given for code, if we are properly support these practitioners in their data-centric endeavors. 

\begin{quote}
\textbf{DD$_5$:}\textit{ Given the centrality of data in the development of DL-based systems, and the effort typically spent on data-related tasks, future research should focus on providing tools and techniques for data analysis that allow developers to better understand their datasets, and how the nature of their data will affect their DL-models, and vice-versa.}
\end{quote}

Once a DL system has been created, developers must then test their system to ensure that it is working as intended. While we discuss the various future directions of work for testing-DL based systems, there is one highly related process that is deeply intertwined with testing: \textit{debugging}. Once developers are able to test their systems, they must then attempt to remedy any detected misbehavior. In traditional software development, this typically means inspecting various aspects of the code, stepping through its execution, and determining failure cases. However, given the data-centric nature of DL systems, "debugging" the \textit{data} will be nearly as important as debugging model, and it is likely that the two tasks will be highly intertwined with one another. Thus, there are clear research opportunities for developing both processes and tools that aid developers in debugging DL-systems across both models and code.

\begin{quote}
\textbf{DD$_6$:}\textit{ There is a clear need for research into understanding and aiding in the debugging of DL-based systems. Such work will need to account for the data-driven nature of the systems and develop tools and techniques for debugging both models and data, and make considerations for collaborative debugging.}
\end{quote}

\subsubsection{Deployment and Monitoring of DL-based systems}
\vspace{0.5em}

Deployment and monitoring practices for traditional software systems have evolved markedly in recently years with advancements related both to processes and infrastructure supporting continuous integration (CI) and deployment (CD). However, it may difficult for such processes to be readily adapted for use in DL-based systems given the size of typical datasets and computational complexity of training DL models. For example, one could envision a CI system that retrains a series of models given updates to a tracked dataset. However, developer would likely need additional monitoring for training processes, and perhaps training processes would differ dynamically based iterative results during the process. Such support could prove difficult for current popular CI systems. Monitoring also poses unique challenges. While there could still exist field failures (akin to crashes) that signal issues with software, it is likely that unwarranted behavior will be more dependent upon end-users reporting anomalous behavior. Additionally, monitoring for performance-related metrics and making recommendations to assuage any potential issues is also likely to be important. These all challenges represent rich areas of research moving forward. 

\begin{quote}
\textbf{DD$_7$:}\textit{ Research should focus on how to adapt current practices for CI/CD to the context of DL-based systems, and develop effective monitoring solutions to capture field failures.}
\end{quote}

\subsubsection{Educational Aspects of the Development and Deployment of DL Systems}
\vspace{0.5em}

It is clear that there are many changes from the traditional software development process reflected in the comparable processes for DL-based systems. As research on these systems continues to evolve, and our understanding advances, the educational materials for students seeking careers in software development or data science must advance alongside it. 

\begin{quote}
\textbf{DD$_8$:}\textit{ As advancements in our understanding of effective development processes and techniques manifest, these must be reflected in freely available educational materials that prepare students accordingly. An academia-industry partnership for such materials would be beneficial to provide practical grounding for course materials.}
\end{quote}

