\section{Testing of Deep Learning Systems}
\label{sec:testing}

\subsection{Background}
\vspace{0.5em}

Testing DL applications using traditional SE infrastructure is hard. They often lack end-to-end formal specifications~\cite{taylor2005rule,seshia2016towards,dreossi2018semantic}. Manually creating a complete specification for complex systems like autonomous vehicles is hard, if not impossible, as it means mimicking all possible real-world scenarios. Researchers from Google Brain observed that while developing DL  applications, a developer ``had in mind a certain (perhaps informally specified) objective or task"; an {\em accident} occurs when the application ``that was designed for that task produced harmful and unexpected results''~\cite{Amodei}. At a conceptual level, these accidents are analogous to semantic bugs in traditional software. Similar to the bug detection and patching cycle in traditional software development, the erroneous behaviors of DNNs, once detected, can be fixed by either adding the error-inducing inputs to the training data set and/or changing the model structure/parameters.

However, a DL is very different from Traditional Software: while human developers manually write the core logic in the former and the logic is encoded in control and data flow, DLs learn their logic from a large amount of training data with minimal human guidance and the logic is encoded in terms of nonlinear activation functions and weights of edges between different neurons. These differences are particularly significant for software testing because testing essentially checks the program logic, which is encoded very differently in these two form of software. For example, code coverage guided testing techniques~\cite{hutchins1994experiments} that rely on control and data-flow logic will likely not work to test DNN logic~\cite{pei2017deepxplore}. Symbolic execution-based testing will also likely be difficult to adapt as it uses SMT solvers that known to have troubles with non-linearity~\cite{cadar2011symbolic}.

To this end, DL system-testing consists of addressing the following four challenges: (1) what components/properties of a DL system should be tested?; (2) how should inputs be generated to test them?; (3) how should progress be measured (akin to measuring testing effectiveness for "traditional software")?; and (4) how should debugging proceed when testing techniques uncover problems?

\subsection{Research Opportunities}
\vspace{0.5em}

\subsubsection{Determining What to Test}
\vspace{0.5em}

The goal of DL system testing is similar to that of exposing defects in traditional software systems. Therefore, the key question that must be answered is: \textit{what constitutes a DL system defect}. Such defects could be present in infrastructure (e.g., TensorFlow), DL application code, data distribution, model structure, weight values, and hyper-parameters. They may have various symptoms, including those similar to software 1.0 bugs such as exceptions/crashes, and others unique to the DL semantics, such as low model accuracy, difficulty in convergence, robustness issues, and malicious back-doors.  For some of these defects, the oracles (intended properties) can be explicitly defined, whereas for others, defining oracle is a prominent challenge. 

\begin{quote}
\textbf{T$_1$:}\textit{ Future work should focus on achieving a better understanding of the faults that might occur in DL-based systems through empirical work.}
\end{quote}

Many DL defects are rooted in low model accuracy. For example, many believe that inaccurate models tend to have robustness issues. Hence, a general test oracle may focus on model accuracy. The challenge is to factor in the discrepancy among data distributions during training, testing, and deployment. Metamorphic testing provides a potential solution by asserting model behaviors upon variations. It is also possible to use the software 1.0 version of the application or an interpretable approximation (e.g., decision tree) as the oracle to test a DL model. Specifically, many DL applications have their antecedents in traditional software, which is based on deterministic algorithms or rules. These algorithms and rules provide an approximation of the intended state space, allowing us to test DL models that are largely uninterpretable. When a DL model is potentially malicious, the properties to test may need to change. Low-level hygiene properties analogous to buffer bound checks in traditional software may need to be tested and validated.

\begin{quote}
\textbf{T$_2$:}\textit{ Researchers should focus on trying to draw analogies between testing practices that have been successfully applied to traditional software systems, and those adapting those to fit the needs of DL-based systems.}
\end{quote}

\subsubsection{Deciding how to Test DL-based Systems}
\vspace{0.5em}

Analogous to testing in traditional software, white-box, black-box, and grey-box testing techniques can be developed to test DL systems. White-box testing is driven by some coverage criteria. A number of such criteria have been proposed in the literature, such as neuron coverage~\cite{Tian:ICSE'18}, and have demonstrated potential in generating diverse inputs. In traditional software, various coverage criteria have different trade-offs in their cost and capabilities in disclosing software defects. For example, definition-use criterion that aim to cover all the dataflow relations in the subject software is much more expensive than statement coverage but much more effective in exposing bugs. Similar trade-offs exist in DL system white-box testing and hence studying their correlations with capabilities of disclosing model defects is of importance. Existing DL model coverage criteria mainly focus on specific model structures (e.g., CNN) and input modality. They can be extended to other structures such as RNN and other modalities. 

\begin{quote}
\textbf{T$_3$:}\textit{ There is a need for the development of proper test adequacy criteria for DL-based systems, akin to code coverage, that align with relevant abstractions of DL-based systems.}
\end{quote}

In traditional software, black-box testing is an important methodology in practice. It does not require access to software implementation or low level design documents. Instead, it directly derives test cases from functional specifications. Existing black-box DL system testing focuses on partitioning a pre-existing data set to training, validation, and test data sets and performs cross-validation. Data augmentation and GAN can be used to generate additional data. However, it is unclear if such data generation can be driven by model functional specifications. 

\begin{quote}
\textbf{T$_4$:}\textit{ Research on evolving practices for Black-box testing of DL systems needs to evolve to provide additional details to developers regarding model performance.}
\end{quote}

Recently, we have witnessed substantial progress in grey-box testing for traditional software systems. Numerous fuzzing techniques and search-based test generation techniques have advanced the state-of-the-art of traditional bug finding. It is likely that similar techniques can be developed to test DL systems. Model continuity and the presence of gradient information provide unique opportunities for such techniques. It is also foreseeable that many effective software 1.0 testing techniques such as mutation testing, unit testing, and regression testing will have their counter-parts in DL system testing. However, in software 1.0, mutating a program statement and testing a function/unit has clear semantics, the un-interpretability of DL models makes it difficult to associate clear meanings to mutating model weight values, model structures, and testing phases (in the pipeline)  and layers in models. There are many reasons to believe that differential testing provides unique benefits as it provides cross-referencing oracles, and leverages counter-factual causality to mitigate the inherent un-interpretability problem in DL systems. Most existing testing techniques focus on testing either DL models or non-model components (in traditional programming languages), co-testing them together as a cohesive system may pose unique challenges.

\begin{quote}
\textbf{T$_4$:}\textit{ There are many promising avenues of potential work on Grey-box testing of DL-systems, particularly related to techniques that take advantage of model continuity and gradient information to drive automated fuzzers or search-based techniques. Researchers should also look to find design counterparts to different types of traditional software testing (e.g. unit tests, regression tests) for DL-based systems.}
\end{quote}

\subsubsection{Determining How to Measure scientific progress on Testing DL-based Systems}
\vspace{0.5em}

Measuring progress is critical as testing is an iterative procedure that cannot expose all the bugs in the test subject. Hence, we need to know that sufficient progress has been made so that the procedure can be terminated. In software 1.0, error detection rate and coverage improvement are used to measure progress. We need to establish the counter-part in DL system testing. Measuring the improvement of model accuracy over time may not be sufficient as the training may fall into some local optima. Measuring model coverage (e.g., neuron coverage) is a plausible solution, although the correspondence between coverage and various model quality objectives needs to be established. Continuous testing after DL system is deployed is valuable as currently rigorous model testing and retraining (after deployment) only happen when things go very wrong. 

\begin{quote}
\textbf{T$_5$:}\textit{ Research into the design of test effectiveness metrics is important to measure the progress being made in testing research for DL-based systems.}
\end{quote}

\subsubsection{Determining How to Debug DL-based Systems}
\vspace{0.5em}

Once defects are disclosed, the subsequent challenge is how to fix these defects. The current practice in DL model engineering relies on trial and error, meaning that the data engineers make changes to various parts such as training data set, model structure and training parameters, which they believe will lead to defect mitigation based on their experience. It lacks a critical step of identifying the root causes of defects and using that as guidance to fix the problems. Effective diagnosis tools to help point to specific (defective) artifacts in the engineering pipeline and suggest possible fixes are hence of importance. Differential analysis that has been highly effective in diagnosing software 1.0 bugs could be valuable in model and infrastructure defect diagnosis. For example, eliminating features or model components and testing how the system performs could be one form of debugging/testing. In addition, while fixing software 1.0 defects largely lies in changing certain program statements, there is hardly a counter-part in fixing model defects. For example, directly changing some weight values have uninterpretable consequences on the model behaviors.  


\begin{quote}
\textbf{T$_6$:}\textit{ Differential analysis may play a key role in aiding in debugging practices for DL-based systems, and near term research could benefit from building on such techniques.}
\end{quote}
